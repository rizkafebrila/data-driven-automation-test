import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Disburse Test Suite')

suiteProperties.put('name', 'Disburse Test Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("E:\\Project\\Katalon\\Download\\Automation Testing Jojonomic\\Reports\\Disburse Test Suite\\20181224_200838\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Disburse Test Suite', suiteProperties, [new TestCaseBinding('Test Cases/Login Test', 'Test Cases/Login Test',  null), new TestCaseBinding('Test Cases/Before Create Transaction Disburse', 'Test Cases/Before Create Transaction Disburse',  null), new TestCaseBinding('Test Cases/Create Disburse Transaction Test Loop - Iteration 1', 'Test Cases/Create Disburse Transaction Test Loop',  [ 'description' : 'Dinas ke Palembang' , 'title' : 'Dinas' , 'amount' : '100000' , 'category' : 'Travel' , 'currency' : 'Indonesian Rupiah (IDR - IDR)' ,  ]), new TestCaseBinding('Test Cases/Create Disburse Transaction Test Loop - Iteration 2', 'Test Cases/Create Disburse Transaction Test Loop',  [ 'description' : 'Pengobatan ke Dokter Gigi' , 'title' : 'Pengobatan' , 'amount' : '200000' , 'category' : 'Medical' , 'currency' : 'Indonesian Rupiah (IDR - IDR)' ,  ]), new TestCaseBinding('Test Cases/Create Disburse Transaction Test Loop - Iteration 3', 'Test Cases/Create Disburse Transaction Test Loop',  [ 'description' : 'Makan Malam saat Lembur' , 'title' : 'Makan Lembur' , 'amount' : '300000' , 'category' : 'Dining' , 'currency' : 'Indonesian Rupiah (IDR - IDR)' ,  ]), new TestCaseBinding('Test Cases/After Create Transaction Disburse', 'Test Cases/After Create Transaction Disburse',  null)])
