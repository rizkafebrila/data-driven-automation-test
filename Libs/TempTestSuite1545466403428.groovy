import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Reimburse Test Suite')

suiteProperties.put('name', 'Reimburse Test Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("E:\\QA\\Automation Testing Jojonomic\\Reports\\Reimburse Test Suite\\20181222_151307\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Reimburse Test Suite', suiteProperties, [new TestCaseBinding('Test Cases/Login Test', 'Test Cases/Login Test',  null), new TestCaseBinding('Test Cases/Create Reimburse Transaction Test - Iteration 1', 'Test Cases/Create Reimburse Transaction Test',  [ 'varAmount' : '100000' , 'varExpense' : 'Foods' , 'varTag' : 'tag - meals' , 'varDesc' : 'Makan Malam' ,  ]), new TestCaseBinding('Test Cases/Create Reimburse Transaction Test - Iteration 2', 'Test Cases/Create Reimburse Transaction Test',  [ 'varAmount' : '200000' , 'varExpense' : 'Transportation' , 'varTag' : 'tag - tele' , 'varDesc' : 'Go-Jek' ,  ]), new TestCaseBinding('Test Cases/Create Reimburse Transaction Test - Iteration 3', 'Test Cases/Create Reimburse Transaction Test',  [ 'varAmount' : '300000' , 'varExpense' : 'Telecommunication' , 'varTag' : 'tag - tele' , 'varDesc' : 'Pulsa' ,  ])])
