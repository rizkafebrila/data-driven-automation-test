<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>open_reimburse_list</name>
   <tag></tag>
   <elementGuidId>5d9c25d2-17f7-4995-bfc2-d8a1cd37575c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'col-md-3 reimbursed']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-3 reimbursed</value>
   </webElementProperties>
</WebElementEntity>
