<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Disburse Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-25T15:31:04</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4600e99b-aaad-429e-97e9-e32733a2ba66</testSuiteGuid>
   <testCaseLink>
      <guid>5a1d80ff-7417-4d63-8641-b9f2f2a9d214</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1df71a12-af9e-41d3-a09d-79d6787d4d34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Before Create Transaction Disburse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>988f8f6f-1210-4c84-810c-cd1b6c8b0ce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Disburse Transaction Test Loop</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>593def03-f7a3-479a-adaa-935285a50a01</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test_Disburse_Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>593def03-f7a3-479a-adaa-935285a50a01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>title</value>
         <variableId>fcf2387d-6119-4045-a106-df0f644f1b03</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>593def03-f7a3-479a-adaa-935285a50a01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>category</value>
         <variableId>7d60f908-3242-404d-8951-950b9d9b9fd1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>593def03-f7a3-479a-adaa-935285a50a01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>amount</value>
         <variableId>1a5e7d58-1b33-42fa-8a85-5d7ef821d59e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>593def03-f7a3-479a-adaa-935285a50a01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>currency</value>
         <variableId>7ec109f3-ad99-40c4-be7b-5552c63a3800</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>593def03-f7a3-479a-adaa-935285a50a01</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>description</value>
         <variableId>2f331082-74a1-4efc-ba9d-a6115752c2af</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ccae2b4b-7147-4562-aa26-f77618113dec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Create Transaction Disburse</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
