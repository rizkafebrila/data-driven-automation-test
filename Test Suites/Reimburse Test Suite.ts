<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Reimburse Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-25T15:35:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7d1266e4-56a5-42c7-93f4-583380e35b16</testSuiteGuid>
   <testCaseLink>
      <guid>fcdbca15-49aa-4954-b0f9-14ce356e2d5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b21deb27-92dc-4f0e-88c6-f887baa28870</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Before Create Transaction Reimburse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>298af622-4d5b-4d91-9605-5711f77ba3e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Reimburse Transaction Test Loop</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d3f0cd79-5515-4b29-889e-d69034040155</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test_Reimburse_Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d3f0cd79-5515-4b29-889e-d69034040155</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>amount</value>
         <variableId>97d27b2c-66e5-4f4c-9260-436dc777e4df</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d3f0cd79-5515-4b29-889e-d69034040155</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expense_category</value>
         <variableId>6732bd71-a886-4c7b-bd93-8a41b1178f17</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d3f0cd79-5515-4b29-889e-d69034040155</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tag_category</value>
         <variableId>c7ae8ee1-074c-4b10-97e1-2b0ad948fd84</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d3f0cd79-5515-4b29-889e-d69034040155</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>description</value>
         <variableId>481075cd-3ff1-44f6-b436-2cc46a47258f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9ad4b19f-acbc-4505-b708-5e4aa9a5f6ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Create Transaction Reimburse</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fd6a7df7-17bc-4079-9752-1eb8afce91d5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
