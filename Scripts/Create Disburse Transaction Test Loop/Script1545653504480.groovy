import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

WebUI.click(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/Create Cash Advance'))

WebUI.delay(2)

WebUI.setText(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/Input_Title'), title)

WebUI.selectOptionByLabel(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/input_select_category'), category, 
    false)

WebUI.delay(2)

WebUI.setText(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/input_Budget_amount'), amount)

WebUI.selectOptionByLabel(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/Input_Currency'), currency, false)

WebUI.delay(2)

WebUI.setText(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/Input_Description'), description)

WebUI.click(findTestObject('Create_Disburse_Page/Page_Jojonomic Pro/button_save_transaction'))

