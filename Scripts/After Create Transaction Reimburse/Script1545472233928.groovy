import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

TestData data = findTestData('Test_Reimburse_Data')

WebUI.delay(2)

WebUI.check(findTestObject('Create_Reimburse_Page/checkbox_claimer'))

for (int i = 2; i <= data.getRowNumbers(); i++) {
	TestObject to = findTestObject('Create_Reimburse_Page/checkbox_loop', [('row') : i])
	
	WebUI.scrollToElement(to, 3)
	
    WebUI.delay(3)

    WebUI.check(to)
}

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_claimer'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/error_message'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_claimer'))

WebUI.delay(3)

WebUI.click(findTestObject('Create_Reimburse_Page/drawer_reimbursement_approver'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/checkbox_approver'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_approve'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/drawer_reimbursement_finance'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/checkbox_finance'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_reimburse'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/radio_cash_resimburse'))

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_cash_reimburse'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/button_submit_reimburse_final'))

WebUI.delay(2)

WebUI.click(findTestObject('Create_Reimburse_Page/open_reimburse_list'))

